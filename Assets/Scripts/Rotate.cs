﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject cube;
    private float speed = 50.0f;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        cube.transform.Rotate(speed * Time.deltaTime * Vector3.up);
    }
}
