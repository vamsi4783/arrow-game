﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSpawn : MonoBehaviour
{
    public Transform[] spawnPoint;
    public GameObject[] Arrow;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSpawning());
    }

    // Update is called once per frame
    IEnumerator StartSpawning()
    {
        yield return new WaitForSeconds(1);

        for (int i = 0; i < 10; i++)

        {
            int randomNumber = Mathf.RoundToInt(Random.Range(0f, spawnPoint.Length - 1));
            Instantiate(Arrow[i], spawnPoint[i].position, Quaternion.identity);
        }

        StartCoroutine(StartSpawning());
    }
    void Update()
    {

    }
}
