﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow2 : MonoBehaviour
{
    public GameObject Green;
    float time;
    float timeDelay;

    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(new Vector3(90, 0, 0), Space.World);
        time = 0f;
        timeDelay = 5f;
    }


    // Update is called once per frame
    void Update()
    {
        time = time + 1f * Time.deltaTime;
        if (time >= timeDelay)
        {
            transform.position = Vector3.MoveTowards(transform.position, Green.transform.position, 10f * Time.deltaTime);
        }
        else
        {
            return;
        }
    }
}
