﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow1 : MonoBehaviour
{
    public GameObject Blue;
    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(new Vector3(90, 0, 0), Space.World);
    }

    // Update is called once per frame
    void Update()
    {
      //  transform.Rotate(new Vector3(90, 0, 0), Space.World);
        transform.position = Vector3.MoveTowards(transform.position, Blue.transform.position, 10f * Time.deltaTime);
    }
}
