﻿using UnityEngine;
using System.Collections;

public class BoardPositioner : MonoBehaviour
{

    public GameObject Blue;
    public GameObject Red;
    public GameObject Green;

    void Start()
    {
        {
            Vector3 position = new Vector3(Random.Range(-25.5f, -25.5f), -11.1f, Random.Range(3.1f, 3.1f));
            Instantiate(Blue, position, Quaternion.identity);
        }
        {
            Vector3 position = new Vector3(Random.Range(8.0f, 8.0f), -11.1f, Random.Range(3.1f, 3.1f));
            Instantiate(Red, position, Quaternion.identity);
        }
        {
            Vector3 position = new Vector3(Random.Range(42.9f, 42.9f), -11.1f, Random.Range(3.1f, 3.1f));
            Instantiate(Green, position, Quaternion.identity);
        }
    }
    void Update()
    {
        
    }
}